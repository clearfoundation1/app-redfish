<?php

/**
 * Redfish ethernet interfaces class.
 *
 * @category   apps
 * @package    redfish
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\redfish;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('redfish');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\redfish\Redfish_Engine as Redfish_Engine;

clearos_load_library('redfish/Redfish_Engine');

// Exceptions
//-----------

use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Redfish ethernet interfaces class.
 *
 * @category   apps
 * @package    redfish
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

class Ethernet_Interfaces_Library extends Redfish_Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Ethernet interfaces constructor.
     *
     * @param string $profile Redfish profile
     */

    public function __construct($profile)
    {
        clearos_profile(__METHOD__, __LINE__);

        parent::__construct($profile);
    }

    /**
     * Returns ethernet interfaces information. 
     *
     * @param string $id system ID
     *
     * @return array information for given ID
     */

    public function get_info($id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Validation
        $validator = new Validation_Exception();
        $validator->check('ID', $this->validate_system_id($id));
        $validator->validate();

        // REST API request to Redfish
        $response = $this->_request('Systems/' . $id . '/EthernetInterfaces');
        $payload = $response['body'];

        $member_urls = $this->_get_memeber_urls($payload->Members);

        $payload = array();
        $items = array();
        
        foreach ($member_urls as $key => $value) {
            $response = $this->_request($value);
            
            $items[] = $response['body'];
        }
        $payload['list'] = $items;
        return $payload;
    }

    /**
     * Returns list of ethernet interfaces systems.
     *
     * @return array list of ethernet interfaces systems
     */

    public function get_list()
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_member_ids('Managers');
    }
}
