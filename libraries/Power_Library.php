<?php

/**
 * Redfish power class.
 *
 * @category   apps
 * @package    redfish
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\redfish;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('redfish');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\redfish\Redfish_Engine as Redfish_Engine;

clearos_load_library('redfish/Redfish_Engine');

// Exceptions
//-----------

use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Redfish power class.
 *
 * @category   apps
 * @package    redfish
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

class Power_Library extends Redfish_Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const STATE_ON = 'On';
    const STATE_GRACEFUL_SHUTDOWN = 'GracefulShutdown';
    const STATE_GRACEFUL_RESTART = 'GracefulRestart';
    const STATE_FORCE_ON = 'ForceOn';
    const STATE_FORCE_OFF = 'ForceOff';
    const STATE_FORCE_RESTART = 'ForceRestart';
    const STATE_PUSH_POWER_BUTTON = 'PushPowerButton';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    protected $reset_types = array();

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Power constructor.
     *
     * @param string $profile Redfish profile
     */

    public function __construct($profile)
    {
        clearos_profile(__METHOD__, __LINE__);

        //// We are not using below array because now we fetched it dynamically
        $this->reset_types = [
            self::STATE_ON => lang('redfish_on'),
            //self::STATE_GRACEFUL_SHUTDOWN => lang('redfish_graceful_shutdown'),
            //self::STATE_GRACEFUL_RESTART => lang('redfish_graceful_restart'),
            self::STATE_FORCE_ON => lang('redfish_force_on'),
            self::STATE_FORCE_OFF => lang('redfish_force_off'),
            self::STATE_FORCE_RESTART => lang('redfish_force_restart'),
            self::STATE_PUSH_POWER_BUTTON => lang('redfish_push_power_button'),
        ];

        parent::__construct($profile);
    }

    /**
     * Returns power information. 
     *
     * @param string $id system ID
     *
     * @return array information for given ID
     */

    public function get_info($id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Validation
        $validator = new Validation_Exception();
        $validator->check('ID', $this->validate_system_id($id));
        $validator->validate();

        // REST API request to Redfish
        $response = $this->_request('Chassis/' . $id . '/Power');
        $payload = $response['body'];

        // Transform/trim response data
        $info['PowerSupplies'] = $payload->PowerSupplies;
        $info['PowerControl'] = $payload->PowerControl;
        if (isset($payload->Oem->Hp->SNMPPowerThresholdAlert))
            $info['SNMPPowerThresholdAlert'] = $payload->Oem->Hp->SNMPPowerThresholdAlert;

        return $info;
    }

    /**
     * Returns list of power systems.
     *
     * @return array list of power systems
     */

    public function get_list()
    {
        clearos_profile(__METHOD__, __LINE__);

        return $this->_member_ids('Chassis');
    }

    /**
     * Returns data options.
     *
     * @param string $id   system ID
     *
     * @return array list of options
     */

    public function get_options($id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Validation
        $validator = new Validation_Exception();
        $validator->check('ID', $this->validate_system_id($id));
        $validator->validate();

        $options['ResetType'] = $this->get_reset_types($id);

        return $options;
    }

    /**
     * Returns Power Options.
     *
     * @param string $id   system ID
     *
     * @return array list of reset types
     */

    public function get_reset_types($id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Validation
        $validator = new Validation_Exception();
        $validator->check('ID', $this->validate_system_id($id));
        $validator->validate();

        $response = $this->_request('Systems/' . $id);

        $power_options = $response['body']->Actions->{'#ComputerSystem.Reset'}->{'ResetType@Redfish.AllowableValues'};

        $reset_types = array();

        foreach ($power_options as $key => $value) {
            
            $reset_types[$value] = $value;
        }
        return $reset_types;
    }

    /**
     * Power reset action.
     *
     * @param string $id   system ID
     * @param string $type reset type
     *
     * @return void
     */

    public function reset($id, $type)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Validation
        $validator = new Validation_Exception();
        $validator->check('ID', $this->validate_system_id($id));
        $validator->check('ResetType', $this->validate_reset_type($type, $id));
        $validator->validate();

        // Request
        $body['ResetType'] = $type;
        $this->_request('Systems/' . $id . '/Actions/ComputerSystem.Reset/', 'post', $body);
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validation routine for reset type.
     *
     * @param string $type reset type
     * @param int $id id
     *
     * @return string error message if reset type is invalid
     */

    public function validate_reset_type($type, $id)
    {
        clearos_profile(__METHOD__, __LINE__);

        if (! array_key_exists($type, $this->get_reset_types($id)))
            return lang('redfish_reset_type_invalid');
    }
}
