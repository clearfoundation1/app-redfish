<?php

/**
 * Redfish class.
 *
 * @category   apps
 * @package    redfish
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\redfish;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('redfish');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Folder as Folder;

use \Exception as Exception;
use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\File_No_Match_Exception as File_No_Match_Exception;
use \clearos\apps\base\File_Not_Found_Exception as File_Not_Found_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Engine');
clearos_load_library('base/File');
clearos_load_library('base/Folder');

clearos_load_library('base/Engine_Exception');
clearos_load_library('base/File_No_Match_Exception');
clearos_load_library('base/File_Not_Found_Exception');
clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Redfish class.
 *
 * @category   apps
 * @package    redfish
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

class Redfish extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const PATH_CONFIG = '/etc/clearos/redfish.d';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    protected $config = array();

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Redfish constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Returns settings for given profile.
     *
     * @param string $profile Redfish profile
     *
     * @return array settings for given profile
     * @throws Engine_Exception
     */

    public function get_settings($profile)
    {
        clearos_profile(__METHOD__, __LINE__);

        $servers = $this->get_servers();

        if (!empty($servers[$profile]))
            return $servers[$profile];
        else
            return [];
    }

    /**
     * Returns configured targets.
     *
     * @return array target server listing
     * @throws Engine_Exception
     */

    public function get_servers()
    {
        clearos_profile(__METHOD__, __LINE__);

        $config = $this->_load_config();

        return $config['servers'];
    }

    /**
     * Update Profile.
     *
     * @param string $profile Profile Name
     * @param string $address Server Address
     * @param string $description Description
     * @param string $username Username
     * @param string $password Password
     * 
     * @return 
     * @throws Engine_Exception
     */
    public function add_profile($profile, $address, $description, $username, $password)
    {
        clearos_profile(__METHOD__, __LINE__);


        $exists_profile = $this->get_settings($profile);

        if ($exists_profile) {
            throw new Engine_Exception(lang('redfish_profile_already_exists'));
            return FALSE;
        }

        // Validate
        // --------

        Validation_Exception::is_valid($this->validate_address($profile));
        Validation_Exception::is_valid($this->validate_address($address));
        Validation_Exception::is_valid($this->validate_description($description));
        Validation_Exception::is_valid($this->validate_username($username));
        Validation_Exception::is_valid($this->validate_password($password));

        $this->_add_profile($profile, $address, $description, $username, $password);
    }
    /**
     * Update Profile.
     *
     * @param string $profile Profile Name
     * @param string $address Server Address
     * @param string $description Description
     * @param string $username Username
     * @param string $password Password
     * 
     * @return 
     * @throws Engine_Exception
     */
    public function update_profile($profile, $address, $description, $username, $password)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Validate
        // --------

        Validation_Exception::is_valid($this->validate_address($address));
        Validation_Exception::is_valid($this->validate_description($description));
        Validation_Exception::is_valid($this->validate_username($username));
        Validation_Exception::is_valid($this->validate_password($password));

        $this->_delete_profile($profile);
        $this->_add_profile($profile, $address, $description, $username, $password);
    } 
    /**
     * Delete Profile.
     *
     * @param string $profile Profile Name
     * 
     * @return 
     * @throws Engine_Exception
     */
    public function delete_profile($profile)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Validate
        // --------

        $this->_delete_profile($profile);
    } 

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validation for profile.
     *
     * @param string $profile profile
     *
     * @return mixed void if profile is valid, errmsg otherwise
     */
    function validate_profile($profile)
    {
        clearos_profile(__METHOD__, __LINE__);
        if (!trim($profile))
            return lang('redfish_invalid_profile');
    }
    /**
     * Validation for address.
     *
     * @param string $address address
     *
     * @return mixed void if address is valid, errmsg otherwise
     */
    function validate_address($address)
    {
        clearos_profile(__METHOD__, __LINE__);
        if (!trim($address))
            return lang('redfish_invalid_address');

        if (!preg_match("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", $address) && !preg_match("^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$", $address)) {

            //return lang('redfish_invalid_address');
        }
    }

    /**
     * Validation for description.
     *
     * @param string $description description
     *
     * @return mixed void if description is valid, errmsg otherwise
     */
    function validate_description($description)
    {
        clearos_profile(__METHOD__, __LINE__);
        if (!trim($description))
            return lang('redfish_invalid_description');
    }

    /**
     * Validation for username.
     *
     * @param string $username username
     *
     * @return mixed void if username is valid, errmsg otherwise
     */
    function validate_username($username)
    {
        clearos_profile(__METHOD__, __LINE__);
        if (!trim($username))
            return lang('redfish_invalid_username');
    }

    /**
     * Validation for password.
     *
     * @param string $password password
     *
     * @return mixed void if password is valid, errmsg otherwise
     */
    function validate_password($password)
    {
        clearos_profile(__METHOD__, __LINE__);
        if (!trim($password))
            return lang('redfish_invalid_password');
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R I V A T E  M E T H O D S 
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Loads configuration.
     *
     * @return void
     * @throws Engine_Exception
     */

    private function _load_config()
    {
        clearos_profile(__METHOD__, __LINE__);

        $valid_keys = ['address', 'description', 'username', 'password'];

        $config_folder = new Folder(self::PATH_CONFIG);

        if (!$config_folder->exists())
            return [];

        $config_files = $config_folder->get_listing();

        foreach ($config_files as $config_file) {
            if (!preg_match('/\.conf$/', $config_file))
                continue;

            $basename = preg_replace('/\.conf$/', '', $config_file);
            $configlet = new File(self::PATH_CONFIG . '/' . $config_file, TRUE);
            $lines = $configlet->get_contents_as_array();

            foreach ($lines as $line) {
                $matches = [];
                if (preg_match('/(\w+)\s*=\s*(.*)/', $line, $matches)) {
                    if (in_array($matches[1], $valid_keys))
                        $config['servers'][$basename][$matches[1]] = $matches[2];
                }
            }
        }

        return $config;
    }
    /**
     * Delete Profile.
     *
     * @param string $profile Profile
     *
     * @return mixed void if profile is valid, errmsg otherwise
     */
    function _delete_profile($profile)
    {
        clearos_profile(__METHOD__, __LINE__);
        
        $file = new File($this->_get_file_path($profile), TRUE);

        if ($file->exists()) {
            $file->delete();
        }
    }
    
    /**
     * Add Profile.
     *
     * @param string $profile Profile Name
     * @param string $address Server Address
     * @param string $description Description
     * @param string $username Username
     * @param string $password Password
     *
     * @return mixed void if profile is valid, errmsg otherwise
     */
    function _add_profile($profile, $address, $description, $username, $password)
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new File($this->_get_file_path($profile), TRUE);

        if (!$file->exists()) {
            $file->create('root', 'root', 644);

            $lines[] = "address = ".$address;
            $lines[] = "description = ".$description;
            $lines[] = "username = ".$username;
            $lines[] = "password = ".$password;
            $file->dump_contents_from_array($lines);
        }
    }
    function _get_file_path($profile)
    {
        return self::PATH_CONFIG .'/'. $profile.'.conf';
    }
}
