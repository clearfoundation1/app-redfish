<?php

/**
 * Redfish class.
 *
 * @category   apps
 * @package    redfish
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\redfish;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('redfish');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\Rest_Client as Rest_Client;
use \clearos\apps\redfish\Redfish as Redfish;

clearos_load_library('base/Engine');
clearos_load_library('base/Rest_Client');
clearos_load_library('redfish/Redfish');

// Exceptions
//-----------

use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\Forbidden_Exception as Forbidden_Exception;
use \clearos\apps\base\Not_Found_Exception as Not_Found_Exception;
use \clearos\apps\base\Unauthorized_Exception as Unauthorized_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;
use \clearos\apps\base\Client_Request_Exception as Client_Request_Exception;

clearos_load_library('base/Engine_Exception');
clearos_load_library('base/Forbidden_Exception');
clearos_load_library('base/Not_Found_Exception');
clearos_load_library('base/Unauthorized_Exception');
clearos_load_library('base/Validation_Exception');
clearos_load_library('base/Client_Request_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Redfish class.
 *
 * @category   apps
 * @package    redfish
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

class Redfish_Engine extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    protected $profile = ''; 

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Redfish constructor.
     *
     * @param string $profile Redfish profile
     */

    public function __construct($profile)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Validation
        $validator = new Validation_Exception();
        $validator->check('profile', $this->validate_profile($profile));
        $validator->validate();

        $this->profile = $profile;
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validation routine for profile.
     *
     * @param string $profile profile name
     *
     * @return string error message if profile is invalid
     */

    public function validate_profile($profile)
    {
        clearos_profile(__METHOD__, __LINE__);

        $redfish = new Redfish();

        $servers = $redfish->get_servers();

        if (! array_key_exists($profile, $servers))
            return lang('redfish_profile_invalid');
    }

    /**
     * Validation routine for system ID.
     *
     * @param string $id system ID
     *
     * @return string error message if system ID is invalid
     */

    public function validate_system_id($id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // FIXME: what's a valid ID in Redfish?
        if (!preg_match('/^[a-zA-Z0-9\.\-_]+$/', $id))
            return lang('redfish_system_id_invalid');
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R I V A T E  M E T H O D S 
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Returns member IDs for given base route.
     *
     * The Redfish API returns a common format for listing members in a group.
     * This is a common method for extracting IDs from these groups.
     *
     * @param array $route base route
     *
     * @return array list of IDs
     */

    protected function _member_ids($route)
    {
        clearos_profile(__METHOD__, __LINE__);

        // REST API request to Redfish
        $response = $this->_request($route . '/');
        $payload = $response['body'];

        $members = $payload->Members;
        $ids = [];

        foreach ($members as $item) {
            $id = preg_replace("/\/redfish\/v1\/$route\//", '', $item->{'@odata.id'});
            $id = preg_replace('/\/$/', '', $id);

            $ids[] = $id;
        }

        return $ids;
    }

    /**
     * Returns member URLs for given Member array.
     *
     * The Redfish API returns a common format for listing members in a group.
     * This is a common method for extracting URls from these groups.
     *
     * @param array $members array members
     *
     * @return array list of Member URLs
     */
    protected function _get_memeber_urls($members)
    {
        clearos_profile(__METHOD__, __LINE__);

        foreach ($members as $item) {
            $url = preg_replace("/\/redfish\/v1\//", '', $item->{'@odata.id'});

            $urls[] = $url;
        }
        return $urls;
    }

    /**
     * Common entrypoint for making API requests to Redfish endpoints.
     *
     * Exceptions should be handled by the caller.
     *
     * @param string $route API route
     * @param string $type  request type
     * @param string $body  request body
     *
     * @return array API response
     */

    protected function _request($route, $type = 'get', $body = '')
    {
        clearos_profile(__METHOD__, __LINE__);

        $redfish = new Redfish();
        $client = new Rest_Client();
        
        $settings = $redfish->get_settings($this->profile);
        $api_servers = [ 'https://' . $settings['address'] ];

        $options['verify'] = FALSE;
        $options['username'] = $settings['username'];
        $options['password'] = $settings['password'];
        $options['body'] = $body;
        $options['type'] = $type;
        $this->_check_server_status($settings['address']);

        $response = $client->request($api_servers, '/redfish/v1/' . $route, $options);
        
        if ($response['http_status_code'] == Rest_Client::HTTP_CODE_UNAUTHORIZED) {
            throw new Unauthorized_Exception();
        } else if ($response['http_status_code'] == Rest_Client::HTTP_CODE_FORBIDDEN) {
            throw new Forbidden_Exception();
        } else if ($response['http_status_code'] == Rest_Client::HTTP_CODE_NOT_FOUND) {
            throw new Not_Found_Exception(lang('redfish_error_404'));
        } else if ($response['http_status_code'] == Rest_Client::HTTP_CODE_VALIDATION_ERROR) {
            throw new Validation_Exception();
        } else if (preg_match('/^4/', $response['http_status_code'])) {
            // See if we can find enhanced error message in the Redfish response.
            $status = json_decode($response['body']);

            if (isset($status->error) && isset($status->error->{'@Message.ExtendedInfo'})) {
                $first_error = $status->error->{'@Message.ExtendedInfo'}[0];
                if (isset($first_error->MessageArgs))
                $error_message = $first_error->MessageArgs[0];
            } else {
                $error_message = $response['status_message'];
            }

            throw new Client_Request_Exception($error_message);
        } else if ($response['http_status_code'] != Rest_Client::HTTP_CODE_OK) {
            throw new Engine_Exception($response['status_message']);
        }

        // It looks like everything is JSON
        $response['body'] = json_decode($response['body']);

        return $response;
    }

    /**
     * Common entrypoint for making API requests to Redfish endpoints.
     *
     * Exceptions should be handled by the caller.
     *
     * @param string $url resource URL
     *
     * @return boolean TRUE if connected
     */

    protected function _check_server_status($url)
    {
        if ($socket =@ fsockopen($url, 80, $errno, $errstr, 4)) {
            fclose($socket);
            return TRUE;
        
        } else {

            throw new Not_Found_Exception(lang('redfish_error_timeout'));
        }
    }
}
