<?php

/**
 * Redfish thermal API controller.
 *
 * @category   apps
 * @package    redfish
 * @subpackage rest-api
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\redfish\Thermal_Library as Thermal_Library;

clearos_load_library('redfish/Thermal_Library');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Redfish thermal API controller.
 *
 * @category   apps
 * @package    redfish
 * @subpackage rest-api
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/redfish/
 */

class Thermal extends ClearOS_REST_Controller
{
    /**
     * Redfish thermal overview.
     *
     * @param string $profile Redfish profile
     * @param string $id      system ID
     *
     * @return view
     */

    function index_get($profile, $id = NULL)
    {
        try {
            $thermal = new Thermal_Library($profile);

            if (is_null($id))
                $data = $thermal->get_list();
            else
                $data = $thermal->get_info($id);

            $this->respond_success($data);
        } catch (\Exception $e) {
            $this->exception_handler($e);
        }
    }
}
