
Name: app-redfish
Epoch: 1
Version: 1.0.16
Release: 1%{dist}
Summary: Redfish Engine - API
License: LGPLv3
Group: Applications/API
Packager: ClearFoundation
Vendor: ClearFoundation
Source: app-redfish-%{version}.tar.gz
Buildarch: noarch

%description
Redfish is a standard API for managing and scaling server deployments.

%package core
Summary: Redfish Engine - API
Requires: app-base-core
Requires: app-base-core >= 1:2.5.25
Requires: app-network-core
Requires: clearos-framework >= 7.5.21

%description core
Redfish is a standard API for managing and scaling server deployments.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/redfish
cp -r * %{buildroot}/usr/clearos/apps/redfish/

install -d -m 0755 %{buildroot}/etc/clearos/redfish.d
install -d -m 0755 %{buildroot}/var/clearos/redfish
install -d -m 0755 %{buildroot}/var/clearos/redfish/backup

%post core
logger -p local6.notice -t installer 'app-redfish-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/redfish/deploy/install ] && /usr/clearos/apps/redfish/deploy/install
fi

[ -x /usr/clearos/apps/redfish/deploy/upgrade ] && /usr/clearos/apps/redfish/deploy/upgrade

exit 0

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-redfish-api - uninstalling'
    [ -x /usr/clearos/apps/redfish/deploy/uninstall ] && /usr/clearos/apps/redfish/deploy/uninstall
fi

exit 0

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/redfish/packaging
%exclude /usr/clearos/apps/redfish/unify.json
%dir /usr/clearos/apps/redfish
%dir /etc/clearos/redfish.d
%dir /var/clearos/redfish
%dir /var/clearos/redfish/backup
/usr/clearos/apps/redfish/deploy
/usr/clearos/apps/redfish/language
/usr/clearos/apps/redfish/api
/usr/clearos/apps/redfish/libraries
