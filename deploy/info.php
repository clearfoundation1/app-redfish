<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'redfish';
$app['version'] = '1.0.16';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('redfish_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('redfish_app_name');
$app['category'] = lang('base_category_system');
$app['subcategory'] = lang('base_subcategory_infrastructure');
$app['menu_enabled'] = FALSE;

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_only'] =  TRUE;

$app['core_requires'] = array(
    'app-base-core >= 1:2.5.25',
    'app-network-core',
    'clearos-framework >= 7.5.21',
);

$app['core_directory_manifest'] = array(
    '/etc/clearos/redfish.d' => array(),
    '/var/clearos/redfish' => array(),
    '/var/clearos/redfish/backup' => array(),
);
